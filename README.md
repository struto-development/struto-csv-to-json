# Usage

This command line utility is available as a single Windows executable (see dist folder, or
functional-test folder). It transforms any CSV file to an array of json objects:

```./struto-csv-to-json.exe [options] <csvFilePath>```

If need be, it can be very easily cross-compiled for Linux.

Apology for non-idiomatic Struto language (Go) included. It is just for completeness.

## Authenticity

SHA1: ```337ae07fe8d60c97ddea9190f7f66ca03e5c70c8```

Verify from command prompt:
```certutil -hashfile struto-csv-to-json.exe```

## Options

**--pretty** - if present, it will create a pretty/indented JSON file

**--separator** - comma || semicolon

## Example

(see functional-test above)

```./struto-csv-to-json.exe --pretty --separator=comma csv-blog-import-sample.csv```

# How to reproduce the build

## Get dependencies

There are actually no dependencies, it's all standard library, but compiler still requires
go.mod to be present.

```
go mod init
go mod tidy
go mod vendor
```

## Strip debug info and compile compact executable

go build -ldflags="-s -w"

## Cross-compiling

See https://medium.com/@utranand/building-golang-package-for-linux-from-windows-22fa23764808

# Further work

This is the most generic any-csv-to-json transformer.

For specific json objects that guarantee the correct format and can eventually be POSTed to
Hubspot, strongly-typed data structure is advised, as in the following contrived example:

```
// Post structure
type Post struct {
      Author          string `json:"AUTHOR"` 
      FeaturedImage   string `json:"FEATURED_IMAGE"`
      MetaDescription string `json:"META_DESCRIPTION"`
      PostBody        string `json:"POST_BODY"`
      PostUrl         string `json:"POST_URL"`
      PublishDate     string `json:"PUBLISH_DATE"`
      SeoTitle        string `json:"SEO_TITLE"`
      Tags            string `json:"TAGS"`
      Title           string `json:"TITLE"`
}

func post() {
    post := Post{"John Doe", "https://via.placeholder.com/640x480?text=Just+Read+It+(TM)", "Some meta info...", (...) }
    jsonReq, err := json.Marshal(post)

    endpoint := "https://api.hubapi.com/cms/v3/blogs/posts?hapikey=YOUR_HUBSPOT_API_KEY"
    resp, err := http.Post(endpoint, "application/json; charset=utf-8", bytes.NewBuffer(jsonReq))
    if err != nil {
        log.Fatalln(err)
    }

    defer resp.Body.Close()
    bodyBytes, _ := ioutil.ReadAll(resp.Body)

    // Convert response body to string
    bodyString := string(bodyBytes)

    // Convert response body to the same Post structure defined above
    var postStruct Post
    json.Unmarshal(bodyBytes, &postStruct)
    fmt.Printf("%+v\n", postStruct)
}
```
